$(function(){
    /*
    var ButAll =true;
    var ButUnity = false;
    var ButUE4 = false;
    var ButWEB = false;
    var ButGamJam = false;
    var ButModelisation = false;
    */
    
    
    

    $(".extand").hide();
    $(".nameSkill").css("opacity", "0");
    
    var $PortfolioItems = $("div .PF").children("div");
    var openedDiv = null;
    
    /*nav bar highlight */
    $(".navbar a, footer a").on("click", function(event){
    
        event.preventDefault();
        var hash = this.hash;
        console.log($(hash).offset().top);
        $('body,html').animate({scrollTop: $(hash).offset().top} , 900 , function(){window.location.hash = hash;})
        
    });

    /*contact form*/
    $('#contact-form').submit(function(e) {
        e.preventDefault();
        $('.comments').empty();
        var postdata = $('#contact-form').serialize();
        
        $.ajax({
            type: 'POST',
            url: 'contact.php',
            data: postdata,
            dataType: 'json',
            success: function(json) {
                 
                if(json.isSuccess) 
                {
                    $('#contact-form').append("<p class='thank-you'>Votre message a bien été envoyé. Merci de m'avoir contacté :)</p>");
                    $('#contact-form')[0].reset();
                }
                else
                {
                    $('#firstname + .comments').html(json.firstnameError);
                    $('#name + .comments').html(json.nameError);
                    $('#email + .comments').html(json.emailError);
                    $('#phone + .comments').html(json.phoneError);
                    $('#message + .comments').html(json.messageError);
                }                
            }
        });
    });

})

function skillIn(elem)
    {
        var chose = elem;
        for(var i=0; i< chose.children.length;i++){
            if(i==1){
                $(chose.children[i]).css("opacity", "1");
            }
        }
    }
    
    function skillOut(elem){
        var chose = elem;
        for(var i=0; i< chose.children.length;i++){
            if(i==1){
                $(chose.children[i]).css("opacity", "0");
            }
        }
    }

function PFButton(elem){
    var But = $(".btnPF");
    for(var i=0 ; i<But.length ;i++){
     if(But[i].classList.contains("btnIsClicked")){
            
            $(But[i]).removeClass("btnIsClicked");
        }
    }
    $("div .PFConstruct").hide();
    $("div .PFWeb").hide();
    $("div .PFUnity").hide();
    $("div .PFGameJam").hide();
    $("div .PFUE4").hide();
    $("div .PFModelisation").hide();
    
    $(elem).addClass("btnIsClicked");
    
    
    if(elem.classList.contains("All")){
        
        $("div .PFWeb").show();
        $("div .PFUnity").show();
        $("div .PFGameJam").show();
        $("div .PFUE4").show();
        $("div .PFModelisation").show();
        $("div .PFConstruct").show();
        
    }else if(elem.classList.contains("Web")){
        
        $("div .PFWeb").show();
        
    }else if(elem.classList.contains("Unity")){
        
        $("div .PFUnity").show();
        
    }else if(elem.classList.contains("GameJam")){
        
        $("div .PFGameJam").show();
        
    }else if(elem.classList.contains("UE4")){
        
        $("div .PFUE4").show();
        
    }else if(elem.classList.contains("Modelisation")){
        
        $("div .PFModelisation").show();
        
    }else if(elem.classList.contains("Construct")){
             $("div .PFConstruct").show();
             }
}